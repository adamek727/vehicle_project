# Vehicle Project

This git contains matherials about differential chassis vehicle

PCB_odometry_vehicle 	- contains SolidWorks model and visualisations of PCB pack
			- PCB design

code			- C code for STM32F4 Deiscovery kit to read encoder s and push them thrue UART

matlab			- odometry model in matlab with position drawing

batery_pack_model	- SolidWorks model and visualisation for battery pack

All the code, that calculates vehicle motion model can be found in main.c file, where constants are delcared from line no. 74 and the calculation itself takes place on 129th line and few next.
(code/Src/main.c)