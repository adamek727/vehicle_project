Partlist exported from E:/school/master_thesis_repo/apendix/PCB_odometry_vehicle/discovery_shield_enders.sch at 3/6/2017 6:22 PM

Part      Value          Device            Package    Description                                      MF    MPN        OC_FARNELL OC_NEWARK SPICEMODEL SPICEPREFIX SPICETYPE
AUX_LEX                  LED_E             LED_1206   LED                                                                                    NONE       D           diode    
BT_UART   22-23-2041     22-23-2041        22-23-2041 .100" (2.54mm) Center Header - 4 Pin             MOLEX 22-23-2041 1462920    38C0355                                   
C1        47n            CPOL-EUA/3216-18W A/3216-18W POLARIZED CAPACITOR, European symbol                                                                                   
C2        330n           CPOL-EUA/3216-18W A/3216-18W POLARIZED CAPACITOR, European symbol                                                                                   
C3        330n           CPOL-EUA/3216-18W A/3216-18W POLARIZED CAPACITOR, European symbol                                                                                   
C4        330n           CPOL-EUA/3216-18W A/3216-18W POLARIZED CAPACITOR, European symbol                                                                                   
D2                       DIODE-DO214AA     DO214AA    DIODE                                                                                                                  
GPS       22-23-2041     22-23-2041        22-23-2041 .100" (2.54mm) Center Header - 4 Pin             MOLEX 22-23-2041 1462920    38C0355                                   
IC1       MAX3232ESE     MAX3232ESE        SO16       True RS-232 Transceivers 3.0V to 5.5V, Low-Power MAXIM MAX3232ESE 9725539    79C3880                                   
IC2       7805DT         7805DT            TO252      Positive VOLTAGE REGULATOR                                                                                             
J1                       JACK-PLUG0        SPC4077    DC POWER JACK                                                     unknown    unknown                                   
JP1                      PINHD-2X25        2X25       PIN HEADER                                                                                                             
JP2                      PINHD-2X25        2X25       PIN HEADER                                                                                                             
PWR_LED                  LED_E             LED_1206   LED                                                                                    NONE       D           diode    
R1        10k            RR1206            R1206      RESISTOR, European symbol                                                              NONE       R                    
R2        10k            RR1206            R1206      RESISTOR, European symbol                                                              NONE       R                    
R3        10k            RR1206            R1206      RESISTOR, European symbol                                                              NONE       R                    
STATE_LED                LED_E             LED_1206   LED                                                                                    NONE       D           diode    
X1        D-SUB9-H3M09RA D-SUB9-H3M09RA    H3M09RA    D-Subminiatur Connector                                           unknown    unknown                                   
X2        MAB7SH         MAB7SH            MAB7SH     Female CONNECTOR                                                  unknown    unknown                                   
X3        22-23-2021     22-23-2021        22-23-2021 .100" (2.54mm) Center Header - 2 Pin             MOLEX 22-23-2021 1462926    25C3832                                   
X5        MAB7SH         MAB7SH            MAB7SH     Female CONNECTOR                                                  unknown    unknown                                   
