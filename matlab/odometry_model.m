clear all;
close all;
clc;

%% Model Constants
wheelbase = 0.571;
circumference = 3.1415 * 0.2606;
pulsesPerRotation = 10000;

%% Model states
fi = 0;
x = 0;
y = 0;

encoderL = 0;
distanceL = 0;
lastDistanceL = 0;
encoderR = 0;
distanceR = 0;
lastDistanceR = 0;
totalTime = 0;
totalDistance = 0;
encoderL_offset = 0;
encoderR_offset = 0;
history = [];

figure
grid on;                                        

% Prepare Serial Channel
newobjs = instrfind
if length(newobjs) > 0
    fclose(newobjs)
end
% s1 = serial('COM3','BaudRate',115200);
% fopen(s1)

s1 = tcpip('10.0.108.1', 8888, 'NetworkRole', 'client');
fopen(s1)

plot_counter = 0;
while (true)
    
    % read data
    string = fgetl(s1);
    tic;
    
    % parse data
    encoders = strsplit(string,',');
    if length(encoders) < 3
        continue
    end
    
    encoderR =  str2num(encoders{2});
    encoderL =  str2num(encoders{1});
    
    if plot_counter == 0
        encoderR_offset = encoderR;
        encoderL_offset = encoderL;
    end
    
    distanceL = ( encoderL-encoderL_offset) / pulsesPerRotation * circumference;
    distanceR = (-encoderR+encoderR_offset) / pulsesPerRotation * circumference;
    

    deltaR = distanceR-lastDistanceR;
    deltaL = distanceL-lastDistanceL;
    
    avgDelta = (deltaR+deltaL) / 2;
    deltaFi = (deltaR - deltaL) / wheelbase;
    
    % calculate diff eq
    fi = fi + deltaFi(1);
    x = x + avgDelta * cos(fi + deltaFi/2);
    y = y + avgDelta * sin(fi + deltaFi/2);
    totalDistance = totalDistance + avgDelta;
    
    % update internal states
    lastDistanceL = distanceL;
    lastDistanceR = distanceR;
    
    % save history
    
    %plot
    if mod(plot_counter,5)==0
        
        hold off
        
        for i = 1 : size(history,2)
            plot(history(1,:), history (2,:), 'b*')
            if i > 1
                hold on
            end
        end
        plot(x,y,'or');
        hold on;
        plot([x x+cos(fi)*0.3],[y y+sin(fi)*0.3],'r' )
        axis([-2,2,-2,2]);
        grid on;
        drawnow;
    end
    
    if mod(plot_counter,100) == 0  
        history = [history [x;y]];
    end
    plot_counter = plot_counter + 1;
    
    % printout results
    %[encoderL, encoderR, delta_t, distanceL, distanceR, fi, x, y]
    [distanceL distanceR, fi]
    %encoderL
end

delete(s)
clear s