clear all;
close all;
clc;

fi = 0;
x = 0;
y = 0;
history = [];

% Prepare Serial Channel
newobjs = instrfind
if length(newobjs) > 0
    fclose(newobjs)
end
%s1 = serial('COM3','BaudRate',115200);
%fopen(s1)

s1 = tcpip('10.0.108.1', 8888, 'NetworkRole', 'client');
fopen(s1)


plot_counter = 0;
while (true)
    
    % read data
    string = fgetl(s1);
    
    % parse data
    encoders = strsplit(string,',');
    if length(encoders) < 3
        continue
    end
    
    x =  str2num(encoders{1});
    y =  str2num(encoders{2});
    fi = str2num(encoders{3});
    

    %plot
    if mod(plot_counter,5)==0
        
        hold off
        
        for i = 1 : size(history,2)
            plot(history(1,:), history (2,:), 'b*')
            if i > 1
                hold on
            end
        end
        plot(x,y,'or');
        hold on;
        plot([x x+cos(fi)*0.3],[y y+sin(fi)*0.2],'r' )
        axis([min(min(-1,x-1),min(-1,y-1)),max(max(1,x+1),max(1,y+1)),min(min(-1,x-1),min(-1,y-1)),max(max(1,x+1),max(1,y+1))]);
        grid on;
        drawnow;
    end
    
    if mod(plot_counter,100) == 0  
        history = [history [x;y]];
    end
    plot_counter = plot_counter + 1;
    
end

delete(s)
clear s